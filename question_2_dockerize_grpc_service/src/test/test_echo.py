import grpc
import pytest

from src.lib import echo_pb2_grpc
from src.lib.echo_pb2 import EchoInputRequest, EchoRandomNoiseRequest


@pytest.fixture()
def echo_client():
    channel = grpc.insecure_channel('localhost:50051')
    stub = echo_pb2_grpc.EchoAPIStub(channel)
    return stub


def test_echo_input(echo_client):
    suffix = 'no'
    prefix = 'yes,'
    middle = 'yes, '

    result = echo_client.EchoInput(
        request=EchoInputRequest(
            user_provided_prefix=prefix,
            user_input=middle,
            user_provided_suffix=suffix
        )
    )

    assert result.user_input_echoed == prefix + middle + suffix


def test_echo_input_invalid_argument(echo_client):
    suffix = 'no'
    middle = None
    prefix = 'yes, '

    try:
        echo_client.EchoInput(
            request=EchoInputRequest(
                user_provided_prefix=prefix,
                user_input=middle,
                user_provided_suffix=suffix
            )
        )
        # wont reach, because the rpc error will have raise from the stub.
        assert False
    except grpc.RpcError as e:
        assert 'was empty!' in e.details()
        assert e.code() == grpc.StatusCode.INVALID_ARGUMENT


def test_echo_random_noise(echo_client):
    result = echo_client.EchoRandomNoise(
        request=EchoRandomNoiseRequest()
    )

    print(result)
    assert result.random_noise