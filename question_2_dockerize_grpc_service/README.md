# Question 2:


## Intro:

### Pre-requirements:
 - pipenv.
 - python >= 3.6 (we recommend using pyenv to manage this).
 - Docker

Located in the `src` directory is a grpc server written in Python3.

Its a simple service that echoes back user input or returns a UUID.


### Verifying Env:

At the root of this project (the same directory this README is in),
there are two scripts, `run_server.sh` & `run_tests.sh`. To make sure the 
server is working on your system, first run `run_server.sh`; this will
boot the server (note that this is blocking). In a second session, run 
`run_tests.sh`. You should get a pytest response of 3/3 tests passing.


## The Task:

Please dockerize this service via a docker file and create a script to
construct said image, stand it up, and run the test battery against it.
The tests try to test against localhost at port `50051`, so please
make sure the server is reachable through via that url while running
as a container.


The only requirement uses only bash complaint syntax (preferably plain `sh`).

## Extra Credit:
 - Repeat this task, but use your solution service to question 1
