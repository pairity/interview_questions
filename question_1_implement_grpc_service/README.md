# Question 1:

Making a service.

## Intro:

### Pre-requirements:
 - Some way to manage python dependencies (we recommend pipenv).
 - python >= 3.6 (we recommend using pyenv to manage this).
 
## The Task:
Please implement the server specified in `RecipeAPI.proto`.
Also write at least 1 test for each end point to verify it works.
For definition of each end points, please refer to comments in the 
proto file itself.

The initial requirement for the service does not need persistent storage
(IE: recipes disappearing between restarts is acceptable).

Both the booting of the service and the running of its tests should be 
runnable via a shell script.

### Hint:
If you are having trouble, look at question 2 and come back to this one.

## Extra Credit:
 - Make the recipes persistent across restart.
 - Add an endpoint to the proto document and implement it.
